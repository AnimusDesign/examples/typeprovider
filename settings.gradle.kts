import java.net.URI

enableFeaturePreview("GRADLE_METADATA")
rootProject.name = "TypeProviderTutorial"

pluginManagement {
    resolutionStrategy {
        eachPlugin {
            if (requested.id.id.startsWith("org.jetbrains.dokka")) {
                // Update Version Build Source if being changed.
                useVersion("0.10.0")
            }
            if (requested.id.id.startsWith("org.jetbrains.kotlin.") || requested.id.id.startsWith("org.jetbrains.kotlin.plugin.serialization")) {
                // Update Version Build Source if being changed.
                useVersion("1.3.71")
            }
            if (requested.id.id.startsWith("design.animus.kotlin.example.typeprovider")) {
                useVersion("0.1.0")
            }
        }
    }
    repositories {
        mavenLocal()
        mavenCentral()
        jcenter()
        gradlePluginPortal()
        maven { url = java.net.URI("https://dl.bintray.com/kotlin/kotlinx") }

    }
}


include(":plugin:")
include(":example:")
