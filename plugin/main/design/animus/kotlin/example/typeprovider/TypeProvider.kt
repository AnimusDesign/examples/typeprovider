package design.animus.kotlin.example.typeprovider

import com.squareup.kotlinpoet.*
import kotlinx.coroutines.runBlocking
import org.gradle.api.Plugin
import org.gradle.api.Project
import java.nio.file.Paths

open class TypeProviderConfig {
    var nameSpace: String = "design.animus.tutorials.typeprovider"
    var targetDirectory: String = "./"
}

class TypeProvider : Plugin<Project> {
    override fun apply(project: Project) {
        val extension = project.extensions.create<TypeProviderConfig>(
                "TypeProviderConfig",
                TypeProviderConfig::class.java
        )
        project.task("generateTypes").doLast {
            val file = FileSpec.builder(extension.nameSpace, "TypeProviderSample")
            val sampleClass = TypeSpec.Companion.classBuilder("User")
                    .addModifiers(KModifier.DATA)
                    .primaryConstructor(
                            FunSpec.constructorBuilder()
                                    .addParameter(
                                            ParameterSpec.builder("id", Int::class)
                                                    .build()
                                    )
                                    .build()
                    )
                    .addProperty(
                            PropertySpec.builder("id", Int::class)
                                    .initializer("id")
                                    .build()
                    )
                    .build()
            file.addType(sampleClass)
                    .build()
                    .writeTo(Paths.get(extension.targetDirectory))

        }
    }
}