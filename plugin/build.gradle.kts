import design.animus.kotlin.frm.Versions.Dependencies
import java.net.URI

plugins {
    kotlin("jvm")
    id("maven-publish")
    id("com.gradle.plugin-publish") version "0.10.1"
    id("java-gradle-plugin")
}
val artifactName = "sample-typeprovider"

kotlin {
    target {
        mavenPublication {
            artifactId = artifactName
        }
    }
    sourceSets {
        main {
            kotlin.setSrcDirs(
                    mutableListOf("main", "generated")
            )
            resources.srcDir("main/resources")
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8:${Dependencies.kotlin}")
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${Dependencies.coroutine}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:${Dependencies.serialization}")
                implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
                implementation("com.squareup:kotlinpoet:${Dependencies.kotlinPoet}")
                implementation("org.jetbrains.kotlin:kotlin-reflect:${Dependencies.kotlin}")
            }
        }
        test {
            kotlin.srcDir("test")
            resources.srcDir("test/resources")
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation(kotlin("test-junit"))
                implementation(kotlin("test"))
            }
        }
    }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            this.groupId = group.toString()
            this.version = version
            artifactId = artifactName
            from(components["java"])
            repositories {
                maven {
                    url = URI(extra["artifactUrl"].toString())
                    credentials(HttpHeaderCredentials::class) {
                        name = "Job-Token"
                        value = System.getenv("CI_JOB_TOKEN")
                    }
                    authentication {
                        register("header", HttpHeaderAuthentication::class)
                    }
                }
            }
        }
    }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

gradlePlugin {
    plugins {
        create(artifactName) {
            id = "design.animus.kotlin.example.typeprovider"
            implementationClass = "design.animus.kotlin.example.typeprovider.TypeProvider"
        }
    }
}

pluginBundle {
    website = "http://musings.animus.design"
    vcsUrl = "https://gitlab.com/AnimusDesign/examples/TypeProvider"
    description = "Sample project to build a type provider.."
    tags = listOf("kotlin", "database", "multiplatform")
}
