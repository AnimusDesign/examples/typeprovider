package design.animus.kotlin.example.typeproviders

import org.junit.Test
import design.animus.tutorials.typeprovider.User
import kotlin.test.assertFails
import kotlin.test.assertTrue

class TestUserType {
    @Test
    fun testUserType() {
        val userOne = User(1)
        val userTwo = User(2)
        assertTrue { userOne.id == 1 }
        assertTrue { userTwo.id == 2 }
    }
}