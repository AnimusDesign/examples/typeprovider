#!/usr/bin/env bash
sed -i 's/include(":example:")/\/\/include\(":example:"\)/g' settings.gradle.kts
echo "Building plugin"
./gradlew :plugin:publishToMavenLocal
./gradlew :plugin:publishMavenPublicationToMavenLocal
./gradlew :plugin:publishPluginMavenPublicationToMavenLocal
./gradlew :plugin:publishSample-typeproviderPluginMarkerMavenPublicationToMavenLocal
./gradlew :plugin:publishToMavenLocal